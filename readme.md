# Widgetify

Widgetify is a production tool which has a list of widgets you can choose to use and makes your life easier with small tools in your browser like, context bookmarks

## Contributor Julian Bertsch


I worked on the Core Application which implements the Client Repository and the Server Repository, there I built the Authorization System to create Roles let the microservice ask for permissions and create them, which is linked to User or to the Role. I also created the Database Structure for that microservice ontop Alejandro's Authentication Algorithm.
I implemented the graphql API, for Roles, Scopes and Widgets I created the Relationship.

In the frontend I built the Layout and the moveable objects, I also built the resizing of Widgets and  

The Widgets: WhatTodo, Linksets and Timezones are made by me

Cloud Structure,
I created the base for our Cloud Computing Kubernetes Structure and set up together with Alejandro the deployment, which builts automatically the docker image, and enrolls it to the kubernetes environment. I created the Configfiles and the Dockerfiles for Client and for the Widgets I wrote

To Sum it up
- Authentication Microservice Architecture for Permissions, Scope and Rolemanagement
- Cloud Architecture
  - Setup GCP
  - Care about Automatic Deployment
  - Writing Dockerfiles
  - Writing Kubernetes Config Yaml
  - Optimize Costs of Google Kubernetes (f.E. using preemptible vm's)
- develope Chrome Extension with 2-side communication between application and extension
- Frontend layout
- Frontend Drag & Drop, ordering of Widgets
- Frontend Resizing Widgets
- Widget Timezones
- Widget WhatToDo
- Widget LinkSets



## Repositories
[Server](https://bitbucket.org/widgetify/server/src/master/)

[Client](https://bitbucket.org/widgetify/client/src/master/)

[Server-Service](https://bitbucket.org/widgetify/service-settings/src/master/)

[Timezone Widget](https://bitbucket.org/widgetify/timezone/src/master/)

[WhatToDo Widget](https://bitbucket.org/widgetify/whattodo/src/master/)